# Ansible Role: systemupdate

Perform a full system update.

## Supported Distributions

- Debian
- Archlinux
- RedHat/CentOS
